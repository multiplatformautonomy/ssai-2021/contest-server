module.exports = {
    configureWebpack: {
        mode: 'production',
        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.runtime.js'
            }
        }
    },
    pages: {
        index: {
            entry: './src/main.js',
            title: 'SSAI Contest'
        }
    }
}
