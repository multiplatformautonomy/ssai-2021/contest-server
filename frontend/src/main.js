import Vue from 'vue'
import App from './App.vue'
import Buefy from 'buefy'
import { ToastProgrammatic as Toast } from 'buefy'
//import 'buefy/dist/buefy.css'
import './assets/contest-style.scss'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import VueRouter from 'vue-router'
import Home from './components/Home.vue'
import Login from './components/Login.vue'
import Register from './components/Register.vue'
import ContestantPortal from './components/ContestantPortal.vue'
import AdminPortal from './components/AdminPortal.vue'
import Scoreboard from './components/Scoreboard.vue'
import EditProfile from './components/EditProfile.vue'

Vue.config.productionTip = false

library.add(fas)
Vue.component('vue-fontawesome', FontAwesomeIcon)
Vue.use(Buefy, {
  // use fontawesome icons
  defaultIconComponent: 'vue-fontawesome',
  defaultIconPack: 'fas',
  customIconPacks: {
    fas: {
      sizes: {
        default: 'lg',
        'is-small': '1x',
        'is-medium': '2x',
        'is-large': '3x'
      },
      iconPrefix: ''
    }
  }
})

// vue-router configuration
Vue.use(VueRouter)

const routes = [
  {
    path: '',
    component: Home
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/register',
    component: Register
  },
  {
    path: '/contestant-portal',
    component: ContestantPortal,
    meta: {
      requiresAuth: true,
      requiresAdmin: false
    }
  },
  {
    path: '/admin-portal',
    component: AdminPortal,
    meta: {
      requiresAuth: true,
      requiresAdmin: true
    }
  },
  {
    path: '/scoreboard',
    component: Scoreboard,
  },
  {
    path: '/profile',
    component: EditProfile,
    meta: {
      requiresAuth: true,
      requiresAdmin: false
    }
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  // handle routing authentication
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // make a GET request to API to see if user is authorized. if so, load page;
    // else, redirect to login
    fetch('/api/user/authed')
      .then((response) => {
        if (response.ok) { 
          // authorized, load page after checking if it needs admin
          if (to.matched.some(record => record.meta.requiresAdmin)) {
            // the desired page needs admin
            fetch('/api/admin/authed')
              .then((adResponse) => {
                if (adResponse.ok) {
                  next()
                } else {
                  // user isn't admin; raise a toast and redirect to contestant portal
                  Toast.open({
                    duration: 3000,
                    message: "You're not authorized to view that page",
                    position: "is-bottom",
                    type: "is-danger",
                    queue: false
                  })
                  next('/contestant-portal')
                }
              })
              .catch((e) => { console.error(e) })
          }
          // page doesn't need admin; load
          next()
        } else {
          // unauthorized, redirect to login
          Toast.open({
            duration: 3000,
            message: "Please log in before viewing that page",
            position: "is-bottom",
            type: "is-danger",
            queue: false
          })
          next('/login')
        }
      })
      .catch((e) => {
        console.error(e)
      })
  } else {
    // page doesn't require auth
    next()
  }
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
