exports.up = async knex => {
    // create schema
    await knex.raw('CREATE SCHEMA IF NOT EXISTS contest');
    // countries
    await knex.schema.withSchema('contest').createTable('countries', function(table) {
        table.increments('id');
        table.string('name').notNullable().unique();
        table.string('abbreviation').notNullable().unique();
        table.string('image_path');
    });
    // teams
    await knex.schema.withSchema('contest').createTable('teams', function(table) {
        table.increments('id');
        table.string('name');
        table.integer('country_id').references('id').inTable('contest.countries');
        table.string('emoji');
        table.integer('leaderboard_position')
        table.string('team_code').unique();
        table.string('latest_submission');
    });
    // users
    await knex.schema.withSchema('contest').createTable('users', function(table) {
        table.increments('id');
        table.string('username').notNullable();
        table.string('passhash').notNullable();
        table.string('first_name').notNullable();
        table.string('middle_name');
        table.string('last_name').notNullable();
        table.string('email').notNullable();
        table.string('github');
        table.string('linkedin');
        table.string('website');
        table.string('phone_number').notNullable();
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.integer('team_id').references('id').inTable('contest.teams');
        table.boolean('is_admin').defaultTo(false).notNullable();
        table.boolean('is_email_confirmed').defaultTo(false).notNullable();
        table.string('email_confirm_code').notNullable();
        table.timestamp('confirm_expires').defaultTo(knex.raw("CURRENT_TIMESTAMP + INTERVAL '1 day'"));
    });
    // submissions
    await knex.schema.withSchema('contest').createTable('submissions', function(table) {
        table.increments('id');
        table.integer('user_id').references('id').inTable('contest.users').notNullable();
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.string('file_path').notNullable();
    });
    // scoreboards
    await knex.schema.withSchema('contest').createTable('scoreboards', function(table) {
        table.increments('id');
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.string('file_path').notNullable();
    });
}

exports.down = async knex => {
    await knex.schema.withSchema('contest').dropTableIfExists('submissions');
    await knex.schema.withSchema('contest').dropTableIfExists('users');
    await knex.schema.withSchema('contest').dropTableIfExists('teams');
    await knex.schema.withSchema('contest').dropTableIfExists('countries');
    await knex.schema.withSchema('contest').dropTableIfExists('scoreboards');
    await knex.raw('DROP SCHEMA IF EXISTS contest');
}
