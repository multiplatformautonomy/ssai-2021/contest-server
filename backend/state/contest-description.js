module.exports = `# Contest Description
This website uses [markdown-it-vue-light](https://github.com/ravenq/markdown-it-vue) to render Markdown. HTML is not allowed, so <strong>something like this</strong> has no effect. Admins may edit this description in the Admin Portal.

## GitHub-style table of contents
[toc]
Note: Only \`h2\` and \`h3\` are shown in the table of contents.
## alter
Markup is similar to fenced code blocks. Valid container types are \`success\`, \`info\`, \`warning\` and \`error\`.
::: success
This is a success block.
:::
::: info
This is an info block.
:::
::: warning
This is a warning block.
:::
::: error
This is an error block.
:::

## Images
![gtri](https://media.glassdoor.com/sqll/247686/georgia-tech-research-institute-squarelogo.png)


## AsciiMath
Inline AsciiMath: \`@(1/2[1-(1/2)^n])/(1-(1/2))=s_n@\`
\`\`\`AsciiMath
oint_Cx^3 dx+4y^2 dy
2=(((3-x)xx2)/(3-x))
sum_(m=1)^oosum_(n=1)^oo(m^2 n)/(3^m(m3^n+n3^m)
\`\`\`
\`\`\`ASCIIMath
phi_n(kappa) = 1/(4pi^2 kappa^2)
 int_0^oo (sin(kappa R))/(kappa R)
 del/(del R)
[R^2 (del D_n (R))/(del R)] del R
\`\`\`
[AsciiMath Documentation](http://asciimath.org/)
## Subscript: H~2~O
You can also use inline math: \`\$H_2O\$\`
## Superscript: 29^th^
You can also use inline math: \`\$29^\{th\}\$\`
## Emoji: :panda_face: :sparkles: :camel: :boom: :pig:
[Emoji Cheat Sheet](http://www.emoji-cheat-sheet.com/)
## Fontawesome: :fa-car: :fa-flag: :fa-bicycle: :fa-leaf: :fa-heart:
[All the Font Awesome icons](http://fontawesome.io/icons/)

## Code blocks
Code highlighting is supported via [highlight-js](https://highlightjs.org/).
### C
\`\`\`c
#include <stdio.h>
int main(int argc char* argv[]) \{
  printf("Hello, World!");
  return 0;
\}
\`\`\`
### python
\`\`\`python
def factorial(n):
    if n == 0 or n == 1:
        return 1
    else:
        return n * factorial(n-1)
\`\`\`
### json
\`\`\`json
\{
  "name": "markdown-it-vue"
\}
\`\`\`
### javascript
\`\`\`javascript
import MarkdownItVue from 'markdown-it-vue'
export default \{
  components: \{
    MarkdownItVue
  \}
\}
\`\`\`
### bash
\`\`\`bash
cd ~
git clone ...
\`\`\`
## Table
| First Header  | Second Header |
| ------------- | ------------- |
| Content Cell  | Content Cell  |
| Content Cell  | Content Cell  |
## Flowchart.js
\`\`\`flowchart.js
st=>start: Start|past:>http://www.google.com[blank]
e=>end: End:>http://www.google.com
op1=>operation: My Operation|past
op2=>operation: Stuff|current
sub1=>subroutine: My Subroutine|invalid
cond=>condition: Yes
or No?|approved:>http://www.google.com
c2=>condition: Good idea|rejected
io=>inputoutput: catch something...|request
para=>parallel: parallel tasks
st->op1(right)->cond
cond(yes, right)->c2
cond(no)->para
c2(true)->io->e
c2(false)->e
para(path1, bottom)->sub1(left)->op1
para(path2, right)->op2->e
st@>op1(\{"stroke":"Red"\})@>cond(\{"stroke":"Red","stroke-width":6,"arrow-end":"classic-wide-long"\})@>c2(\{"stroke":"Red"\})@>op2(\{"stroke":"Red"\})@>e(\{"stroke":"Red"\})
\`\`\``