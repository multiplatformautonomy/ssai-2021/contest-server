function seedCountries(knex) {
    // seed contest.countries table with US and UK
    console.log('seedCountries running');
    return knex('contest.countries')
        .insert([
            { name: 'United States', abbreviation: 'US' },
            { name: 'United Kingdom', abbreviation: 'UK' }
        ])
        .onConflict('abbreviation').ignore()
        .returning('*')
        .then((countries) => {
            console.log(`Added countries (empty means countries already there):`);
            console.log(countries);
        });
}

module.exports = seedCountries;
