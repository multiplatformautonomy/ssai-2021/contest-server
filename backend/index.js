require('dotenv').config();
const fs = require('fs');
const admZip = require('adm-zip');
const fetch = require('node-fetch');
const nodemailer = require("nodemailer");
const fileType = require('file-type');
const express = require('express');
const fileUpload = require('express-fileupload');
const process = require('process');
const port = +process.env.PORT || 80;
const bcrypt = require('bcrypt');
const passport = require('passport');
const flash = require('express-flash');
const session = require('express-session');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const csvParse = require('csv-parse');
const helmet = require('helmet');
const knex = require('knex')({
    client: "pg",
    connection: process.env.DATABASE_URL || {
      host: process.env.DB_HOST || "localhost",
      user: process.env.DB_USERNAME || "root",
      password: process.env.DB_PASSWORD || "password",
      database: process.env.DB_DATABASE || "postgres",
      port: 5432
    }
});
const isEmail = require('validator/lib/isEmail');
const isURL = require('validator/lib/isURL');

let ContestDescription = require('./state/contest-description');
const scriptGen = require('./scriptGen');
let isAdminSubmissionsLoading = false;  // blocks user submission if true
let blackoutDateArray = require('./state/blackout');  // blocks user submission during the times in this array

const initializePassport = require('./passport-config');
// initialize passport local strategy with knex
initializePassport(passport);

const seedCountries = require('./seedCountries');
// initialize US and UK countries
seedCountries(knex);

// auto-generate admin with random password
// const adminSeedMaybe = require('./adminSeedMaybe');
// adminSeedMaybe(knex, bcrypt);

console.log('Starting contest API server...');
const app = express();

// ** app middleware ** //
app.use(helmet({
    // sets Content-Security-Policy, X-Frame-Options, SameSite, etc
    contentSecurityPolicy : {
        directives: {
            "default-src": ["'self'"],
            "font-src": ["'self'", "https://cdn.jsdelivr.net/katex/"],
            "img-src": ["'self'", "*", "data:"],  // allow all images
            "script-src": ["'self'", "https://www.google.com", "https://www.gstatic.com"],
            "style-src": ["'self'", "https://fonts.googleapis.com", "https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css", "'unsafe-inline'"],
            "frame-src": ["'self'", "https://www.google.com"],
            "frame-ancestors": ["'self'", "https://www.google.com", "https://gstatic.com"]
        }
    }
}));

app.use('/', express.static('/contest-server/frontend/dist'));
app.use(cookieParser());
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(fileUpload({
    useTempFiles: true,
    safeFileNames: true,
    preserveExtension: true,
    tempFileDir: '/contest-server/tmp/',
    abortOnLimit: true,
    limits: { fileSize: 6000000000 }  // 6 GB
}));
app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: true,
    secure: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

const rateLimit = require('express-rate-limit');
const createAccountLimiter = rateLimit({
    windowMs: 5 * 60 * 1000,  // 1 hour
    max: 25
});
const uploadLimiter = rateLimit({
    windowMs: 5 * 60 * 1000,  // five minutes
    max: 25
});
const userLimiter = rateLimit({
    // for user endpoints
    windowMs: 5 * 60 * 1000,  // five minutes
    max: 150
});
app.use('/api/user/', userLimiter);  // rate limit user endpoints

// initialize job that deletes users with expired email confirmation
const initDeleteJob = require('./deleteUsers');
initDeleteJob(knex);

// ** static routes ** //
app.get('/confirm/:code', (req, res) => {
    // confirm user's email
    if (req.params.code.toString().match(/[^0-9]/g)) {
        // should only contain numbers
        return res.status(404).send("We couldn't find that confirmation code... could you double-check your email?"); 
    }
    const href = `You can now go to the <a href="https://${process.env.PUBLIC_DOMAIN}">main site</a>.`
    // PUBLIC_DOMAIN must be an absolute URL, like https://www.example.com
    return knex('contest.users')
        .where('email_confirm_code', req.params.code)
        .select('is_email_confirmed', 'id')
        .then((user) => {
            if (user.length !== 1) {
                // user doesn't exist
                res.status(404).send("We couldn't find that confirmation code... could you double-check your email? " + href);
            } else if (user[0].is_email_confirmed) {
                // user exists and is already confirmed
                res.status(200).send("You're already confirmed! Go ahead and log in on the home page. " + href);
            } else {
                // user exists and is about to be confirmed
                return knex('contest.users')
                    .where('id', user[0].id)
                    .update({ is_email_confirmed: true })
                    .then(() => {
                        res.status(200).send("You're confirmed! Go ahead and log in on the home page. " + href);
                    })
                    .catch((e) => {
                        console.error(e);
                        res.status(500).send("Something went wrong... contact an admin and we'll get it sorted out. " + href);
                    });
            }
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send("Something went wrong... contact an admin and we'll get it sorted out. " + href);
        });
});

// ** api endpoints ** //
app.post('/api/user/register', [credentialConflictRegister, createAccountLimiter, recaptcha], (req, res, next) => {
    // create user after checking email, username, and phone aren't in use
    if (!validateCredentials(req.body)) {
        return res.status(405).send('Validation check failed');
    }
    return createUser(req, res)
        .then(() => {
            passport.authenticate('local', (err, user, info) => {
                if (user) {
                    req.login(user, err => {
                        if (err) { console.error(err); }
                        res.status(200).send("Logged in");
                    });
                }
            })(req, res, next);
        })
        .catch((err) => {
            console.error(err);
            res.status(500).send('Internal server error');
        });
});

app.get('/api/user/register/countries', (req, res) => {
    // return countries for register page
    return knex('contest.countries')
        .select()
        .then((countries) => {
            res.status(200).json(countries);
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
});

app.post('/api/user/login', recaptcha, (req, res, next) => {
    passport.authenticate('local', (err, user, info) => {
        if (err) {
            // internal server error
            res.status(500).send('Internal server error');
            return next(err);
        }
        if (!user) {
            // wrong credentials
            return res.status(401).send([user, "Login error", info])
        }
        if (user) {
            // success; frontend redirects to contestant portal
            req.login(user, err => {
                if (err) { console.error(err); }
                res.send("Logged in");
            });
        }
    })(req, res, next);
});

app.get('/api/user/logout', (req, res) => {
    req.logout();
    res.status(200).send('Logged out');
});

app.post('/api/user/resetpassword', (req, res) => {
    if (!req.body.username || req.body.username.match(/[^a-zA-Z0-9]/) || req.body.username.length > 20) {
        return res.status(404).send('Username failed check');
    }
    if (!req.body.email || !isEmail(req.body.email)) {
        return res.status(404).send('Email failed check');
    }
    return knex('contest.users')
        .where('username', req.body.username)
        .where('email', req.body.email)
        .first()
        .then((user) => {
            if (!user) {
                return res.status(404).send('No user');
            }
            const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            let userPassPlain = '';
            for (let i=0; i<13; i++) {
                // randomly generate password
                userPassPlain += chars.charAt(Math.floor(Math.random() * chars.length));
            }
            const userPassHash = bcrypt.hashSync(userPassPlain, 10);
            sendResetEmail(user.email, userPassPlain)
                .then(() => {
                    return knex('contest.users')
                        .where('username', req.body.username)
                        .where('email', req.body.email)
                        .update('passhash', userPassHash)
                        .then(() => {
                            console.log(`Password reset for user ${req.body.username}`);
                            res.status(200).send('Password reset');
                        })
                        .catch((e) => {
                            console.error(e);
                            res.status(500).send('Internal server error');
                        })
                })
                .catch((e) => {
                    console.error(e);
                    res.status(500).send('Internal server error');
                });
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
});

app.get('/api/user/info', isAuthenticated, (req, res) => {
    // return user if the session is authenticated, without passhash
    return knex('contest.users')
        .where('id', req.session.passport.user).select(
            'id',
            'username',
            'first_name',
            'middle_name',
            'last_name',
            'email',
            'github',
            'linkedin',
            'website',
            'phone_number',
            'created_at',
            'team_id'
        ).first()
        .then((user) => {
            res.json({ user: user });
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
});

app.get('/api/user/authed', isAuthenticated, (req, res) => {
    // see if user is logged in. isAuthenticated returns 401 if not
    return res.status(200).send('Authenticated');
});

app.get('/api/user/confirmed', isAuthenticated, (req, res) => {
    // see if user has confirmed their email
    return knex('contest.users')
        .where('id', req.session.passport.user)
        .select('is_email_confirmed', 'confirm_expires')
        .then((confirmed) => {
            confirmed = confirmed[0];
            if (!confirmed.is_email_confirmed) {
                res.status(444).json({ expires: confirmed.confirm_expires })
            } else {
                res.status(200).send('Confirmed');
            }
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
});

app.get('/api/user/email/resend', isAuthenticated, (req, res) => {
    return knex('contest.users')
        .where('id', req.session.passport.user)
        .select('email', 'email_confirm_code')
        .then((data) => {
            let email = data[0].email;
            let code = data[0].email_confirm_code;
            sendVerifyEmail(email, code)
                .then(() => {
                    res.status(200).send('Success');
                })
                .catch((e) => {
                    console.error(e);
                    res.status(500).send('Internal server error');
                })
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        })
})

app.put('/api/user/update', [isAuthenticated, credentialConflictEdit], (req, res) => {
    // update user profile after checking username, email, phone aren't in use and checking password
    if (!validateCredentials(req.body)) {
        return res.status(405).send('Backend validation error');
    }
    return knex('contest.users')
        .where('id', req.session.passport.user)
        .pluck('passhash')
        .then((passhash) => {
            // check password
            let oldHash = passhash[0];
            if (bcrypt.compareSync(req.body.old_password, oldHash)) {
                let newHash = undefined;
                if (req.body.password) {
                    newHash = bcrypt.hashSync(req.body.password, 10);
                }
                return knex('contest.users')
                    .where('id', req.session.passport.user)
                    .update({
                        // hardcoded so it's impossible to change is_admin
                        username: req.body.username,
                        first_name: req.body.first_name,
                        middle_name: req.body.middle_name,
                        last_name: req.body.last_name,
                        email: req.body.email,
                        github: req.body.github,
                        linkedin: req.body.linkedin,
                        website: req.body.website,
                        phone_number: req.body.phone_number,
                        // if user doesn't want to change password, it remains undefined and knex skips it
                        passhash: newHash
                    })
                    .then(() => {
                        res.status(200).send('Profile updated');
                    })
                    .catch((err) => {
                        console.error(err);
                        res.status(500).send('Internal server error');
                    });
            } else {
                res.status(401).send('Password mismatch');
            }
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
});

app.get('/api/contest-description/info', isAuthenticated, (req, res) => {
    // return ContestDescription for admin portal and contestant portal
    return res.status(200).json({ contestDescription: ContestDescription });
});

app.post('/api/contest-description/update', [isAuthenticated, isAdmin], (req, res) => {
    // write new description to contest-description.js
    // update ContestDescription so /api/contest-description/info GETs the right one
    ContestDescription = req.body.contestDescription;
    let escapedDesc = req.body.contestDescription  // escape template string chars
        .replace(/\\/g, "\\\\")
        .replace(/`/g, "\\`")
        .replace(/\$/g, "\\$")
        .replace(/[{}]/g, (match) => match === "{" ? "\\{" : "\\}");
    let newDescription = `module.exports = \`${escapedDesc}\``;
    fs.writeFileSync('./state/contest-description.js', newDescription, (err) => {
        if (err) {
            console.error(err);
            return res.status(500).send('Internal server error');
        }
    });
    return res.status(200).send('Description written');
});

app.get('/api/user/blackout', isAuthenticated, (req, res) => {
    // return the list of blackout dates
    res.status(200).json({ blackoutDates: blackoutDateArray });
});

app.post('/api/user/submit', [isAuthenticated, isInTeam, uploadLimiter, recaptcha], (req, res) => {
    // upload submission to backend. saved under .../data/submissions/[team_id]/[user_id]
    if (isAdminSubmissionsLoading) {
        // return early if admin is processing submissions
        return res.status(503).send('Admin submissions loading');
    }
    if (isInBlackout() && !req.user.is_admin) {
        return res.status(400).send('In blackout');
    }
    return knex('contest.users')
        .where('id', req.session.passport.user)
        .pluck('team_id')
        .then((team) => {
            let teamId = team[0];
            let submissionsPath = `/contest-server/data/submissions/`;
            fs.mkdirSync(submissionsPath, { recursive: true });
            let submission = req.files.submission;
            // move submission. it'll be saved as teamId.zip
            let savePath = `${submissionsPath}${teamId}.zip`;
            let previousSubmissionExists  = false;
            try {
                // in case anything fails downstream, we want to be able to restore the old file
                if (fs.statSync(savePath)) {  // see if there's a previous submission
                    // not sure i need this if in a try
                    previousSubmissionExists = true;
                    fs.renameSync(savePath, `${savePath}_old`);
                }
            } catch(err) {
                // no previous submission; do nothing
                console.log(`no previous submission for team id ${teamId}`);
            } finally {
                submission.mv(savePath)
                          .then(() => {
                              // check file type is zip
                              fileType.fromFile(savePath)
                                      .then((filetype) => {
                                          if (!filetype || filetype.ext !== 'zip' || filetype.mime !== 'application/zip') {
                                              fs.rmSync(savePath);  // delete the non-zip
                                              fs.renameSync(`${savePath}_old`, savePath);  // restore old file
                                              return res.status(405).send('Not a zip');
                                          } else {
                                              // insert new row to submissions table
                                              return knex('contest.submissions')
                                                  .insert({
                                                      user_id: req.session.passport.user,
                                                      file_path: savePath
                                                      // created_at defaults to now
                                                  }).then(() => {
                                                      // update user's team's latest_submission
                                                      return knex('contest.teams')
                                                          .where('id', teamId)
                                                          .update('latest_submission', savePath)
                                                          .then(() => {
                                                              if (previousSubmissionExists) {
                                                                  fs.rmSync(`${savePath}_old`);
                                                              }
                                                              res.status(200).send('Successfully uploaded');
                                                          })
                                                          .catch((e) => {
                                                              console.error(e);
                                                              res.status(500).send('Internal server error');
                                                          });
                                                  }).catch((e) => {
                                                      console.error(e);
                                                      res.status(500).send('Internal server error');
                                                  });
                                          }
                                      })
                                      .catch((e) => {
                                          console.error(e);
                                          res.status(500).send('Internal server error');
                                      });
                          })
                          .catch((e) => {
                              console.error(e);
                              res.status(500).send('Internal server error');
                          });
            }
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
});

app.get('/api/user/gamefilm/exists', [isAuthenticated, isInTeam], (req, res) => {
    return knex('contest.users')
        .where('id', req.session.passport.user)
        .pluck('team_id')
        .then((team) => {
            if (!team || !team[0]) {
                return res.status(512).send("User doesn't have team");
            } else {
                // we expect the game film to be a zip with the name ${team_id}.zip
                fs.access(`/contest-server/data/gamefilm/${team}.zip`, (err) => {
                    if (err) {
                        res.status(404).send('No game film');
                    } else {
                        res.status(200).send('Game film exists');
                    }
                });
            }
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
});

app.get('/api/user/gamefilm/download', [isAuthenticated, isInTeam], (req, res) => {
    return knex('contest.users')
        .where('id', req.session.passport.user)
        .pluck('team_id')
        .then((team) => {
            if (!team || !team[0]) {
                return res.status(512).send("User doesn't have team");
            } else {
                res.download(`/contest-server/data/gamefilm/${team[0]}.zip`, (err) => {
                    if (err) {
                        if (err.code === 'ENOENT') {
                            res.status(404).send('No such file');
                        } else {
                            res.status(500).send('Internal server error');
                        }
                    }
                });
            }
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
});

app.get('/api/admin/authed', [isAuthenticated, isAdmin], (req, res) => {
    // verify user is admin
    return res.status(200).send('admin login');
});

app.get('/api/admin/submissions/:mode/:param/:country', [isAuthenticated, isAdmin], (req, res) => {
    // return ./submissions as a zip (of zips). Regardless of which country is
    // selected, this will download all submissions. However, scriptGen will
    // only match the selected country.
    isAdminSubmissionsLoading = true;
    if (!req.params.param.toString().match(/^[0-9]+$/g)) {
        // should only have numbers
        return res.status(405).send('Invalid param');
    }
    if (req.params.mode !== 'full_round_robin' && req.params.mode !== 'shift' && req.params.mode !== 'friendly_stage') {
        // must be one of them
        return res.status(405).send('Invalid mode');
    }
    if (!req.params.param.toString().match(/^[0-9]+$/g) && req.params.country !== 'all') {
        // must be number or all
        return res.status(405).send('Invalid country');
    }
    let time = saneDate();
    scriptGen(knex, time, req.params.mode, req.params.param, req.params.country)
        .then((response) => {
            if (response === 'error') {
                // probably, there is only one team, so it doesn't make sense to generate a script
                isAdminSubmissionsLoading = false;
                res.status(400).send();
            } else {
                response.on('close', () => {  // TODO maybe delete old zips?
                    fs.mkdirSync('/contest-server/data/submission-zips', { recursive: true });
                    const zipName = `${time}_submissions.zip`;
                    const compressPath = `/contest-server/data/submission-zips/${zipName}`;
                    let zip = new admZip();
                    zip.addLocalFolder('/contest-server/data/submissions');
                    zip.writeZip(compressPath);
                    res.download(compressPath, (err) => {
                        isAdminSubmissionsLoading = false;
                        if (err) {
                            console.error(err);
                            res.status(500).send('Internal server error');
                        }
                    });
                });
            }
        })
        .catch((e) => {
            isAdminSubmissionsLoading = false;
            console.error(e);
            res.status(500).send('Intenal server error');
        });
});

app.get('/api/admin/submissions/table', [isAuthenticated, isAdmin], (req, res) => {
    // return the submissions table for admin viewing
    return knex('contest.submissions')
        .rightJoin('contest.users', 'contest.users.id', 'contest.submissions.user_id')
        .leftJoin('contest.teams', 'contest.teams.id', 'contest.users.team_id')
        .select(
            'user_id',
            'contest.teams.name',
            'contest.submissions.created_at',
            'username',
            'first_name',
            'last_name',
            'file_path',
            'email',
            'team_id',
            'emoji'
        )
        .then((submissions) => {
            // filter for the most recent date for each team. this seems quite inefficient
            let result = {};
            for (let i=0; i<submissions.length; i++) {
                let thisSubmit = submissions[i];
                if (!thisSubmit.user_id || !thisSubmit.team_id) {
                    continue;
                }
                let thisDate = new Date(thisSubmit.created_at);
                if (result[thisSubmit.team_id.toString()]) {
                    if (thisDate > new Date(result[thisSubmit.team_id.toString()].created_at)) {
                        result[thisSubmit.team_id.toString()] = thisSubmit;
                    } else {
                        continue;
                    }
                } else {
                    result[thisSubmit.team_id.toString()] = thisSubmit;
                }
            }
            let output = [];
            for (let key in result) {
                // flatten object
                output.push(result[key]);
            }
            res.status(200).json({ submissions: output });
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
});

app.get('/api/admin/teams/table', [isAuthenticated, isAdmin], (req, res) => {
    // return the teams table for admin
    return knex('contest.teams')
        .select()
        .then((teams) => {
            res.status(200).json({ teams: teams });
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
});

app.get('/api/admin/teams/csv', [isAuthenticated, isAdmin], (req, res) => {
    // return the teams table as a csv for admin
    return knex('contest.teams')
        .select()
        .then((teams) => {
            let stat = makeCsv(teams, './teams.csv');
            if (stat === 0) {
                // success
                res.status(200).download('./teams.csv');
            } else {
                res.status(500).send('Internal server error');
            }
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
});

app.get('/api/admin/users/table', [isAuthenticated, isAdmin], (req, res) => {
    // return the users table for admin
    return knex('contest.users')
        .select(
            'id',
            'username',
            'first_name',
            'middle_name',
            'last_name',
            'email',
            'github',
            'linkedin',
            'website',
            'phone_number',
            'created_at',
            'team_id',
            'is_admin',
            'is_email_confirmed',
            'email_confirm_code',
            'confirm_expires'
        )
        .then((users) => {
            res.status(200).json({ users: users });
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
});

app.get('/api/admin/users/csv', [isAuthenticated, isAdmin], (req, res) => {
    // return the users table as a csv for admin
    return knex('contest.users')
        .select(
            'id',
            'username',
            'first_name',
            'middle_name',
            'last_name',
            'email',
            'github',
            'linkedin',
            'website',
            'phone_number',
            'created_at',
            'team_id',
            'is_admin',
            'is_email_confirmed',
            'email_confirm_code',
            'confirm_expires'
        )
        .then((users) => {
            let stat = makeCsv(users, './users.csv');
            if (stat === 0) {
                res.status(200).download('./users.csv');
            } else {
                res.status(500).send('Internal server error');
            }
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
});

app.post('/api/admin/blackout', [isAuthenticated, isAdmin], (req, res) => {
    // update blackout times
    let newDateArray = [];
    for (let i=0; i<req.body.blackoutDateArray.length; i++) {
        if (req.body.blackoutDateArray[i].length !== 2) {
            return res.status(400).send('Invalid array');
        }
        let newDateStart = new Date(req.body.blackoutDateArray[i][0]);
        let newDateEnd = new Date(req.body.blackoutDateArray[i][1])
        if (newDateStart == "Invalid Date" || newDateEnd == "Invalid Date") {
            // type coercion on purpose
            return res.status(400).send('Array contains a non-date');
        } else {
            newDateArray.push([newDateStart, newDateEnd]);
        }
    }
    blackoutDateArray = newDateArray;
    let blackoutString = `module.exports = ${JSON.stringify(newDateArray)}`;
    fs.writeFileSync('./state/blackout.js', blackoutString, (err) => {
        if (err) {
            console.error(err);
            return res.status(500).send('Internal server error');
        }
    });
    return res.status(200).send('Blackout updated');
});

app.post('/api/admin/make', [isAuthenticated, isAdmin], (req, res) => {
    // make some users admin (called from admin portal)
    let usersToMakeAdmin = req.body.adminUsers;
    let userIds = usersToMakeAdmin.map(user => user.id);
    console.log(`Admin with id ${req.session.passport.user} updated admins to be user ids ${userIds}.`);
    // first, remove admin from users not in list
    return knex('contest.users')
        .whereNotIn('id', userIds)
        .update({ is_admin: false })
        .then(() => {
            // next, add new admins
            return knex('contest.users')
                .whereIn('id', userIds)
                .update({ is_admin: true })
                .then(() => {
                    res.status(200).send('Admins updated');
                })
                .catch((e) => {
                    console.error(e);
                    res.status(500).send('Internal server error');
                });
        });
});

app.get('/api/submissions/team', isAuthenticated, (req, res) => {
    // get latest submission for current user's team
    return knex('contest.users')
        .where('id', req.session.passport.user)
        .pluck('team_id')
        .then((team) => {
            if (!team || !team[0]) {
                // should never happen
                return res.status(512).send("User doesn't have team");
            } else {
                return knex('contest.teams')
                    .where('id', team[0])
                    .pluck('latest_submission')
                    .then((submissionPath) => {
                        res.download(submissionPath[0], (err) => {
                            if (err) {
                                console.error(err);
                                res.status(500).send('Internal server error');
                            }
                        });
                    })
                    .catch((e) => {
                        console.error(e);
                        res.status(500).send('Internal server error');
                    });
            }
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
});

app.get('/api/submissions/team/exists', isAuthenticated, (req, res) => {
    // check to see if current user has a latest team submission
    const teamQuery = knex('contest.users').where('id', req.session.passport.user).select('team_id');
    return knex('contest.teams')
        .where('id', teamQuery)
        .pluck('latest_submission')
        .then((submissionPath) => {
            if (!submissionPath || !submissionPath[0]) {
                res.status(404).send('No latest team submission');
            } else {
                res.status(200).send('Latest team submission exists');
            }
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
});

app.post('/api/attachment/upload', [isAuthenticated, isAdmin, uploadLimiter], (req, res) => {
    // upload attachment for contestants to download.
    // must upload a zip file, and it'll be saved as 'Contest_Info.zip'
    // will overwrite previous uploads
    const attachmentFolder = '/contest-server/data/attachment/';
    fs.mkdirSync(attachmentFolder, { recursive: true });
    let attachment = req.files.attachment;
    attachment.mv(attachmentFolder+'Contest_Info.zip')
              .then(() => {
                  fileType.fromFile(attachmentFolder+'Contest_Info.zip')
                          .then((filetype) => {
                              if (!filetype || filetype.ext !== 'zip' || filetype.mime !== 'application/zip') {
                                  fs.rmSync(attachmentFolder+'Contest_Info.zip');
                                  return res.status(405).send('Not a zip');
                              } else {
                                  res.status(200).send('Successfully uploaded');
                              }
                          })
                          .catch((e) => {
                              console.error(e);
                              res.status(500).send('Internal server error');
                          });
              })
              .catch((e) => {
                  console.error(e);
                  res.status(500).send('Internal server error');
              });
});

app.get('/api/attachment/exists', isAuthenticated, (req, res) => {
    // check if Contest_Info.zip exists
    fs.access('/contest-server/data/attachment/Contest_Info.zip', (err) => {
        if (err) {
            res.status(404).send('No contest info');
        } else {
            res.status(200).send('Contest info exists');
        }
    });
});

app.get('/api/attachment/download', isAuthenticated, (req, res) => {
    // download attachment; called by contestant portal
    res.download('/contest-server/data/attachment/Contest_Info.zip', (err) => {
        if (err) {
            if (err.code === 'ENOENT') {
                // file doesn't exist
                res.status(404).send('No such file');
            } else {
                res.status(500).send('Internal server error');
            }
        }
    });
});

app.post('/api/scoreboards/upload', [isAuthenticated, isAdmin, uploadLimiter], (req, res) => {
    // upload scoreboard, which should be csvs
    const scoreboardFolder = '/contest-server/data/scoreboards/';
    fs.mkdirSync(scoreboardFolder, { recursive: true });
    let scoreboard = req.files.attachment;
    if (scoreboard.mimetype !== 'text/csv') {
        // not foolproof since you can spoof mime type, but file-type doesn't work with csvs
        return res.status(405).send('Not a CSV');
    }
    let scoreboardPath = scoreboardFolder+`${saneDate(true)}_${scoreboard.name}`;
    scoreboard.mv(scoreboardPath)
               .then(() => {
                   return knex('contest.scoreboards')
                       .insert({
                           file_path: scoreboardPath
                       })
                       .then(() => {
                           res.status(200).send('Successfully uploaded');
                       })
                       .catch((e) => {
                           console.error(e);
                           res.status(500).send('Internal server error');
                       });
               })
               .catch((e) => {
                   console.error(e);
                   res.status(500).send('Internal server error');
               });
});

app.get('/api/scoreboards/download/csv/:id', (req, res) => {
    // download the scoreboard as a csv with id :id
    if (req.params.id.toString().match(/^[0-9]+$/g)) {
        return res.status(405).send('Invalid id');
    }
    return knex('contest.scoreboards')
        .where('id', req.params.id)
        .pluck('file_path')
        .then((boardPath) => {
            if (!boardPath || !boardPath[0]) {
                return res.status(404).send('No scoreboards found');
            } else {
                res.download(boardPath[0], (err) => {
                    if (err) {
                        console.error(err);
                        res.status(500).send('Internal server error');
                    }
                });
            }
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
});

app.get('/api/scoreboards/download/array/:id', (req, res) => {
    // download the scoreboard as an array with id :id
    if (req.params.id.toString().match(/^[0-9]+$/g)) {
        return res.status(405).send('Invalid id');
    }
    return knex('contest.scoreboards')
        .where('id', req.params.id)
        .pluck('file_path')
        .then((boardPath) => {
            if (!boardPath || !boardPath[0]) {
                return res.status(404).send('No scoreboards found');
            } else {
                const csvString = fs.readFileSync(boardPath[0], 'utf-8');
                csvParse(csvString, { columns: true }, (err, output) => {
                    if (err) {
                        console.error(err);
                        return res.status(500).send('Internal server error');
                    } else {
                        return res.status(200).json({ data: output });
                    }
                });
            }
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
});

app.get('/api/scoreboards/download/index', (req, res) => {
    // send the ids and created_at of scoreboards
    return knex('contest.scoreboards')
        .select('id', 'created_at')
        .orderBy('created_at', 'desc')  // most recent first
        .then((boards) => {
            return res.status(200).json({ boards: boards });
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
});

app.post('/api/teams/create', [isAuthenticated, validateTeamName], (req, res) => {
    // create a new team (called from register). returns teamCode
    return knex('contest.countries')
        .where('id', req.body.country)
        .first()
        .then((country) => {
            return knex('contest.teams')
                .insert({
                    name: req.body.name,
                    country_id: country.id
                }).returning('id')
                .then((team) => {
                    // team is a singleton array containing the team id
                    let teamCode = genTeamCode(team[0]);
                    return knex('contest.teams')
                        .where('id', team[0])
                        .update({
                            emoji: req.body.emoji,
                            team_code: teamCode
                        })
                        .then(() => {
                            joinTeam(teamCode, req.session.passport.user)
                                .then((response) => {
                                    // callback hell lol
                                    if (response === '404') {
                                        return res.status(404).send('Could not find team');
                                    } else if (response === '500') {
                                        return res.status(500).send('Internal server error');
                                    } else if (response === 0) {
                                        return res.status(409).send('Already in a team');
                                    } else {
                                        return res.status(200).json({ teamCode: teamCode });
                                    }
                                })
                                .catch((e) => {
                                    console.error(e);
                                    return res.status(500).send('Internal server error')
                                })
                        })
                        .catch((e) => {
                            console.error(e);
                            res.status(500).send('Internal server error');
                        });
                });
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
});

app.post('/api/teams/join', isAuthenticated, (req, res) => {
    // join a team by code (called from edit profile)
    joinTeam(req.body.teamCode, req.session.passport.user)
        .then((response) => {
            if (response === '404') {
                return res.status(404).send('Could not find team');
            } else if (response === '500') {
                return res.status(500).send('Internal server error');
            } else if (response === 0) {
                // if response is 0, then user's already in a team
                return res.status(409).send('Already in a team');
            }
            return res.status(200).send('Joined team');
        })
        .catch((e) => {
            console.error(e);
            return res.status(500).send('Internal server error');
        });
});

app.get('/api/teams/info/:idType/:identifier', (req, res) => {
    // get team info (name, emoji) from team_code or team_id
    if (req.params.idType !== 'team_code' && req.params.idType !== 'team_id') {
        return res.status(405).send('Invalid URL');
    }
    return knex('contest.teams')
        .where(req.params.idType, req.params.identifier)
        .returning('*')
        .then((team) => {
            if (team.length > 1) {
                // more than one team with that identifier. should never happen
                return res.status(500).send('Internal server error');
            }
            if (team[0]) {
                res.status(200).json(team[0]);
            } else {
                res.status(404).send('Could not find team');
            }
        })
        .catch((e) => {
            console.error(e);
            return res.status(500).send('Internal server error');
        });
});

app.get('/api/teams/info/user', isAuthenticated, (req, res) => {
    // get team info of current user
    const teamQuery = knex('contest.users').where('id', req.session.passport.user).select('team_id');
    return knex('contest.teams')
        .where('id', teamQuery)
        .returning('*')
        .then((team) => {
            if (team.length > 1) {
                // more than one team with that identifier. should never happen
                return res.status(500).send('Internal server error');
            }
            if (team[0]) {
                res.status(200).json(team[0]);
            } else {
                res.status(404).send('Could not find team');
            }
        })
        .catch((e) => {
            console.error(e);
            return res.status(500).send('Internal server error');
        });
});

app.get('/api/teams/validate/:name', validateTeamName, (req, res) => {
    // validate team name is available
    return res.status(200).send('Team name available');
});

app.get('/api/teams/members', isAuthenticated, (req, res) => {
    // return the submissions of the current user's team's members
    const teamQuery = knex('contest.users').where('id', req.session.passport.user).select('team_id');
    return knex('contest.users')
        .where('team_id', teamQuery)
        .leftJoin('contest.submissions', 'contest.users.id', 'contest.submissions.user_id')
        .select(
            'username',
            'first_name',
            'last_name',
            'contest.submissions.created_at'
        )
        .then((members) => {
            res.status(200).json({ members: members });
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
});


// ** helper functions ** //

function validateCredentials(credObj) {
    // returns whether the credentials in credObj are valid
    let nameRegex = /[^A-zÀ-ÖØ-öø-įĴ-őŔ-žǍ-ǰǴ-ǵǸ-țȞ-ȟȤ-ȳɃɆ-ɏḀ-ẞƀ-ƓƗ-ƚƝ-ơƤ-ƥƫ-ưƲ-ƶẠ-ỿ'\- ]/;
    if (!credObj.username || credObj.username.match(/[^a-zA-Z0-9]/) || credObj.username.length > 20) {
        return false;
    }
    if (!credObj.first_name || credObj.first_name.match(nameRegex)) {
        return false;
    }
    if (credObj.middle_name) {
        if (credObj.middle_name.match(nameRegex)) {
            return false;
        }
    }
    if (!credObj.last_name || credObj.last_name.match(nameRegex)) {
        return false;
    }
    if (!credObj.email || !isEmail(credObj.email)) {
        return false;
    }
    if (credObj.github) {
        if (credObj.github.match(/[^a-zA-Z0-9-]/)) {
            return false;
        }
    }
    if (credObj.linkedin) {
        if (!isURL(credObj.linkedin)) {
            return false;
        }
    }
    if (credObj.website) {
        if (!isURL(credObj.website)) {
            return false;
        }
    }
    if (!credObj.phone_number || credObj.phone_number.match(/[^0-9+ \- ()]/) || credObj.phone_number.length > 19 || credObj.phone_number.length < 10) {
        return false;
    }
    return true;
}

function sendVerifyEmail(rawEmail, code) {
    const domain = process.env["PUBLIC_DOMAIN"]
    let email = rawEmail.replace(/[^a-zA-Z0-9_@\-\.]/g, '');
    const username = process.env["EMAIL_USERNAME"]
    const password = process.env["EMAIL_PASSWORD"]
    const emailTransport = nodemailer.createTransport({
        // TODO: make more generic, currently this just assumes outlook.
        host: "smtp-mail.outlook.com", // hostname
        secureConnection: false, // TLS requires secureConnection to be false
        port: 587, // port for secure SMTP
        tls: {
            ciphers:'SSLv3'
        },
        auth: {
            user: username,
            pass: password
        }
    });
    return emailTransport.sendMail({
        from: username,
        to: email,
        subject: "Contest Registration - Email Verification Link",
        text: `Thank you for registering to participate in the Swarm and Search Artificial Intelligence (SSAI) Challenge Series: Capture The Flag Competition.`+
              `To complete your registration, please enter the following code in the contestant portal:\n${code}\n\n`+
              `Be sure to check labhack.org frequently for updates.\n\nGood luck in this year's competition!`
    });
}

function sendResetEmail(rawEmail, plainPass) {
    const domain = process.env["PUBLIC_DOMAIN"]
    let email = rawEmail.replace(/[^a-zA-Z0-9_@\-\.]/g, '');
    const username = process.env["EMAIL_USERNAME"]
    const password = process.env["EMAIL_PASSWORD"]
    const emailTransport = nodemailer.createTransport({
        // TODO: make more generic, currently this just assumes outlook.
        host: "smtp-mail.outlook.com", // hostname
        secureConnection: false, // TLS requires secureConnection to be false
        port: 587, // port for secure SMTP
        tls: {
            ciphers:'SSLv3'
        },
        auth: {
            user: username,
            pass: password
        }
    });
    return emailTransport.sendMail({
        from: username,
        to: email,
        subject: "Contest Registration - Reset Password",
        text: `We received a request to reset your password for the SSAI Capture the Flag Contestant Portal.`+
	      `If you did not request this change, please contact us at bob.lee@wbi-innovates.com.\n\n`
	      `Your temporary password is ${plainPass}.\n\n`+
	      `Please use this to log in and then change your password immediately on the Edit Profile page.\n`+
	      `Good luck in the competition!`
    });
}

function createUser(req) {
    const hash = bcrypt.hashSync(req.body.password, 10);
    const confirmCode = Math.floor((1 + Math.random()) * 10000000000);
    return new Promise((resolve) => {
        let result = knex('contest.users')
        .insert({
            username: req.body.username,
            passhash: hash,
            first_name: req.body.first_name,
            middle_name: req.body.middle_name,
            last_name: req.body.last_name,
            email: req.body.email,
            github: req.body.github,
            linkedin: req.body.linkedin,
            website: req.body.website,
            phone_number: req.body.phone_number,
            email_confirm_code: confirmCode
        }).returning('*');
        let verify = sendVerifyEmail(req.body.email, confirmCode);
        Promise.all([result,verify]).then(([knexUser, _verify])=> {
            resolve(knexUser);
        });
    });
}

function credentialConflictRegister(req, res, next) {
    // middleware for checking if credentials aren't in use from the register page
    return knex('contest.users')
        .select()
        .where('username', req.body.username)
        .orWhere('email', req.body.email)
        .orWhere('phone_number', req.body.phone_number)
        .then((response) => {
            if (response.length !== 0) {
                return res.status(409).send('Credential conflict');
            } else {
                return next();
            }
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
}

function credentialConflictEdit(req, res, next) {
    // middleware for checking that username, email, and phone number aren't already in use.
    // does not tell user which (username, email, phone number) conflicts for privacy
    return knex('contest.users')
        .select()
        .where('username', req.body.username)
        .orWhere('email', req.body.email)
        .orWhere('phone_number', req.body.phone_number)
        .then((response) => {
            const validator = (accumulator, currentValue) => accumulator && (currentValue.id === req.session.passport.user); 
            if (response.length === 0 || response.reduce(validator, response[0].id === req.session.passport.user)) {
                // no conflicts or all conflicts are the same user
                return next();
            } else {
                return res.status(409).send('Credential conflict');
            }
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
}

function isAuthenticated(req, res, next) {
    // middleware for seeing if a user is authenticated
    if (req.isAuthenticated()) {
        return next();
        // frontend redirects to login
    } else {
        res.status(401).send('not authorized');
    }
}

function isAdmin(req, res, next) {
    // middleware for seeing if a user is an admin
    if (req.isAuthenticated() && req.user.is_admin) {
        return next();
    } else {
        res.status(401).send('not authorized');
    }
}

function saneDate(milliseconds) {
    // returns "[YYYY-M-D]HHhMMmSSs(MILms)", so it's sortable
    // milliseconds is a boolean that appends milliseconds if true
    let date = new Date();
    function pad(n) { return (n > 9) ? n.toString() : '0' + n; }
    function pad3(n) { return (pad(n).length > 2) ? pad(n) : '0' + pad(n); }
    return `[${date.getFullYear()}-${pad(date.getMonth()+1)}-${pad(date.getDate())}]${pad(date.getHours())}h${pad(date.getMinutes())}m${pad(date.getSeconds())}s${milliseconds ? pad3(date.getMilliseconds())+'ms' : ''}`;
}

function genTeamCode(id) {
    // generates a five-digit team code and appends the team_id (to ensure uniqueness)
    let code = '';
    let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for (let i = 0; i < 5; i++) {
        code += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return code + id;
}

function joinTeam(teamCode, userId) {
    // join team with code teamCode for user userId
    if (teamCode.match(/[^A-Z0-9]/) || userId.toString().match(/[^0-9]/)) {
        return new Promise((resolve, reject) => {
            resolve('405');
        });
    }
    return knex('contest.teams')
        .where('team_code', teamCode)
        .returning('id')
        .then((team) => {
            if (team.length < 1) {
                // no team with that code
                return new Promise((resolve, reject) => {
                    // string because the else branch return an int with the team id
                    resolve('404');
                });
            } else if (team.length > 1) {
                // more than one team with that code (should never happen)
                return new Promise((resolve, reject) => {
                    resolve('500');
                });
            } else {
                return knex('contest.users')
                    .where({ 'id': userId })
                    .whereNull('team_id')  // if user's already in a team, do nothing
                    .update('team_id', team[0].id)
            }
        })
        .catch((e) => {
            console.error(e);
            return new Promise((resolve, reject) => {
                resolve('500');
            });
        });
}

function validateTeamName(req, res, next) {
    // middleware for checking that the team name isn't in use
    let name = req.params.name ? req.params.name : req.body.name;
    if (name.match(/[^a-zA-Z0-9 ]/)) {
        return res.status(400).send('Invalid team name');
    }
    return knex('contest.teams')
        .where('name', name)
        .then((response) => {
            if (response.length === 0) {
                return next();
            } else {
                return res.status(405).send('Team name conflict');
            }
        })
        .catch((e) => {
            console.error(e);
            res.status(500).send('Internal server error');
        });
}

function isInTeam(req, res, next) {
    // middleware for checking if user is in a team (they have to be to submit)
    return knex('contest.users')
        .where('id', req.session.passport.user)
        .pluck('team_id')
        .then((teamId) => {
            if (!teamId || !teamId[0]) {
                res.status(404).send('No team');
            } else {
                // check that team is in teams table
                return knex('contest.teams')
                    .where('id', teamId[0])
                    .then((team) => {
                        if (team && team.length !== 0) {
                            next();
                        } else {
                            res.status(404).send('No team');
                        }
                    })
                    .catch(() => res.status(500).send('Internal server error'));
            }
        })
        .catch(() => res.status(500).send('Internal server error'));
}

function recaptcha(req, res, next) {
    // middleware for verifying recaptcha token
    fetch(`https://google.com/recaptcha/api/siteverify?secret=${process.env.RECAPTCHA_SECRET}&response=${req.body.recaptchaToken}`, {
        // verify captcha
        method: 'POST'
    }).then((response) => {
        if (response.ok) {
            response.json().then((json) => {
                if (json.success) {
                    next();
                } else if (json["error-codes"]) {
                    console.error(json["error-codes"]);
                    res.status(500).send('Recaptcha error');
                } else {
                    res.status(401).send('Recaptcha failed');
                }
            }).catch((e) => {
                console.error(e);
                res.status(500).send('Internal server error');
            });
        } else {
            res.status(500).send('Internal server error');
        }
    }).catch((e) => {
        console.error(e);
        res.status(500).send('Internal server error');
    });
}

function isInBlackout() {
    // block requests during blackouts
    let currentDate = new Date();
    for (let i=0; i<blackoutDateArray.length; i++) {
        if (currentDate > new Date(blackoutDateArray[i][0]) && currentDate < new Date(blackoutDateArray[i][1])) {
            return true
        }
    }
    return false
}

function makeCsv(data, loc) {
    // make a csv from data (knex output) and save it to loc
    if (!data[0]) {
        return 1;
    }
    let dataString = '';  // it's not great to do this in a string, but i can't get streams to work
    let cols = Object.keys(data[0]);
    cols.map(col => dataString += `${col},`)
    dataString = dataString.slice(0, -1) + '\n'  // chop off last comma and add newline
    for (let i=0; i<data.length; i++) {
        // add the actual data
        let thisRow = '';
        for (let col in data[i]) {
            thisRow += `${data[i][col]},`
        }
        dataString += thisRow.slice(0, -1) + '\n'
    }
    try {
        fs.writeFileSync(loc, dataString);
        return 0;
    } catch(e) {
        console.error(e);
        return 2;
    }
}


const https = require("https");
https.createServer({
    hostname: "ssai.gtri.gatech.edu",
    key: fs.readFileSync(process.env["SSL_PKEY"]),
    cert: fs.readFileSync(process.env["SSL_CERT"])
}, app).listen(443);

const httpApp = express();
httpApp.use(function(req,res,next) {
    res.redirect(301, `https://${process.env["PUBLIC_DOMAIN"]}/${req.path}`);
});
httpApp.listen(80);

// app.listen(80);
