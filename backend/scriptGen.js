const fs = require('fs');

function scriptGen (knex, time, mode='shift', param, country) {
    // generate a script according to mode. returns a WriteStream, which is
    // handled in /api/admin/submissions/:mode/:param.
    return knex('contest.teams')
        .whereNotNull('latest_submission')
        .orderBy('team_code')  // since team codes are random, this order is random
        .then((teams) => {
            if (teams.length <= 1) {
                // error; less or equal to 1 team doesn't make sense
                return new Promise(resolve => resolve('error'));
            }
            if (country !== "all") {
                // filter to just the countries asked for
                teams = teams.filter(team => country == team.country_id);
            }
            // remove admin submissions
            let admin_team_codes = ["DKCIL1", "KIYNU2", "TPYSP4", "RJJJH5", "UUBHE9", "OKSWA10"];
            teams = teams.filter(team => !admin_team_codes.includes(team.team_code));
            let matches = [];
            if (mode === 'grouped_round_robin') {
                // param represents groupSize
                // TODO implement maybe
                //matches = genGroupRoundRobin(teams, param);
                return new Promise(resolve => resolve('error'));
            } else if (mode === 'full_round_robin') {
                matches = genRoundRobin(teams);
            } else if (mode === 'shift') {
                // param represents week number
                matches = genShift(teams, param);
            } else if (mode === 'friendly_stage') {
                matches = genFriendly(teams, param);
            } else {
                // error
                return new Promise(resolve => resolve('error'));
            }
            // write the file as a csv: teamid1,teamid2
            fs.mkdirSync('/contest-server/data/submissions/csvs/', { recursive: true });
            let stream = fs.createWriteStream(`/contest-server/data/submissions/csvs/${time}_${mode}_${param}_${country}.csv`);
            for (let i = 0; i < matches.length; i++) {
                stream.write(`${matches[i][0]},${matches[i][1]}\n`);
            }
            stream.end();
            return stream;
        });
}

// function genGroupRoundRobin(teams, groupSize) {
//     // FIXME this function is trying to do too much. remove the supergroup case to a different
//     // function.
//     let matches = [];
//     while (teams.length > 0) {
//         console.log('teams:');
//         console.log(teams);
//         let thisGroupSize;
//         let group = [];
//         if (teams.length >= groupSize * 2) {
//             // we have enough teams remaining to use group size
//             thisGroupSize = groupSize;
//         } else {
//             // we don't have enough teams to make two full groups, so just match
//             // everyone remaining
//             thisGroupSize = teams.length;
//         }
//         for (let i = 0; i < thisGroupSize; i++) {
//             // add relative paths to group
//             let I;
//             if (groupSize !== thisGroupSize) {
//                 // we have more than groupSize in this group
//                 I = i;
//             } else {
//                 I = Math.floor(Math.random() * teams.length);
//             }
//             console.log(`I = ${I}`);
//             console.log(`teams[I] = ${teams[I].toString()}`);
//             let path = `..${teams[I].latest_submission.slice(35)}`;
//             group.push(path);
//             if (groupSize === thisGroupSize) {
//                 // remove element at I if we're in the normal case (non-supergroup)
//                 // TODO fix this hacky mess
//                 teams.splice(I, 1);  // remove element at I
//             }
//         }
//         matches.concat(roundRobin(group));
//         if (groupSize !== thisGroupSize) {
//             break;
//         }
//     }
//     console.log('grouped round robin returning matches');
//     console.log(matches);
//     return matches;
//}

function genRoundRobin(teams) {
    // run a full round robin for teams (teams choose 2)
    // [['team_1', 'team_2'], ['team_1', 'team_3'], ['team_2', 'team_3']]
    let matches = teams.flatMap(
        (v, i) => teams.slice(i+1).map( w => ['..'+v.latest_submission.slice(35), '..'+w.latest_submission.slice(35)] )
    );
    return matches;
}

function genShift(teams, week) {
    // shift format (half the teams are home and half are away, and we shift
    // the away teams by 1 every week)
    let teamsNum = teams.length;
    // shift the away array by week weeks. repeats when week = teamsNum/2
    let away = teams.slice(teamsNum-week).concat(teams.slice(0, teamsNum-week));
    let matches = [];
    for (let i = 0; i < teamsNum; i++) {
        let match = [`.${teams[i].id}`, `.${away[i].id}`];
        matches.push(match);
    }
    return matches;
}

function genFriendly(teams, n) {
    // match each team with n other teams, chosen randomly. not guaranteed that there's no repeats
    // and not guaranteed that the matches will be different from week to week. also, non-deterministic.
    // n must be smaller than teams.length
    let teamsNum = teams.length;
    if (n > teamsNum) {
        return;
    }
    let matches = [];
    for (let i = 0; i < teamsNum; i++) {
        // pick matches for every team. i is the current team's index
        let opponents = [];  // list of team ids (not indices)
        for (let j = 0; j < n; j++) {
            // pick n other teams
            let opp = teams[Math.floor(Math.random() * teamsNum)].id;
            while (opponents.includes(opp) || opp === teams[i].id) {
                // make sure there's no duplicates and opponent is not the current team
                opp = teams[Math.floor(Math.random() * teamsNum)].id;
            }
            opponents.push(opp);
        }
        for (let k = 0; k < opponents.length; k++) {
            // push each current team-opponent pair to matches
            matches.push([teams[i].id, opponents[k]]);
        }
    }
    return matches;
}

module.exports = scriptGen;
