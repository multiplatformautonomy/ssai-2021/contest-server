const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const knex = require('knex')({
    client: "pg",
    connection: process.env.DATABASE_URL || {
      host: process.env.DB_HOST || "localhost",
      user: process.env.DB_USERNAME || "root",
      password: process.env.DB_PASSWORD || "password",
      database: process.env.DB_DATABASE || "postgres",
      port: 5432
    }
});

function initialize(passport) {
    passport.serializeUser((user, done) => done(null, user.id));
    passport.deserializeUser((id, done) => {
        knex('contest.users').where({id}).first()
                     .then((user) => { done(null, user) })
                     .catch((e) => { done(err, null) })
    });

    const options = {};
    
    passport.use(new LocalStrategy(options, (username, password, done) => {
        knex('contest.users').where({ username }).first()
                     .then((user) => {
                         if (!user) return done(null, false);
                         if (!bcrypt.compareSync(password, user.passhash)) {
                             return done(null, false);
                         } else {
                             return done(null, user);
                         }
                     })
                     .catch((e) => { return done(e); });
    }));
}

module.exports = initialize;
