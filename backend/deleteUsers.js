const schedule = require('node-schedule');

const rule = new schedule.RecurrenceRule();
rule.tz = 'Etc/UTC';
rule.hour = 0;  // run every UTC midnight
rule.minute = 0;
rule.second = 0;

function initDeleteJob(knex) {
    const deleteUsersJob = schedule.scheduleJob(rule, scanAndDelete(knex));   
}

function scanAndDelete(knex) {
    // return function that scans database for expired email confirmations and deletes expired users
    return () => {
        let date = new Intl.DateTimeFormat([], { dateStyle: 'medium', timeStyle: 'short' })
                           .format(Date.now());
        return knex('contest.users')
            .whereRaw('confirm_expires > CURRENT_TIMESTAMP')
            .andWhere('is_email_confirmed', false)
            .del()
            .then((delCount) => { console.log(`Job at ${date} deleted ${delCount} users`); })
            .catch((e) => { console.error(e); });
    }
}

module.exports = initDeleteJob;
