#!/bin/bash

#npm install --global yarn

# Variable Setup
HOMEDIR=$(dirname "$0")
HOMEDIR=$(realpath $HOMEDIR)
ERRLOG="$HOMEDIR/launch.err"
OUTLOG="$HOMEDIR/launch.out"

# User-customizable Environment Variables with defaults
: ${CONTEST_HOST:=0.0.0.0}
: ${CONTEST_PORT:=7147}
: ${CONTEST_MODE:=development}

# echo -e "Starting contest $CONTEST_MODE server on $CONTEST_HOST:$CONTEST_PORT." | tee -a $ERRLOG | tee -a $OUTLOG;
cd $HOMEDIR/frontend
yarn install
yarn lint --fix
yarn build

echo "Launching backend web server:"
cd $HOMEDIR/backend
yarn install
# set up knex migration: have to set MIGRATE to true or false, like this:
# MIGRATE=true docker-compose up [--build]. Defaults to false
if [ $MIGRATE = 'true' ]; then
   echo "Migrating database..."
   npx knex migrate:latest
else
    echo "Not migrating database"
fi
yarn serve
