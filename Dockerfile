FROM node:14

RUN mkdir /contest-server /contest-server/data
COPY frontend /contest-server/frontend
COPY backend /contest-server/backend
COPY launch.sh /contest-server/launch.sh
RUN chmod a+x /contest-server/*.sh

EXPOSE 3000

CMD [ "/contest-server/launch.sh" ]
