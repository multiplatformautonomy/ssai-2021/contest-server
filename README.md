# Contest Server

This server runs a head-to-head reinforcement learning competition using SCRIMMAGE and MASS.

## How to get started
1. Clone the repo
        
        git clone https://gitlab.com/multiplatformautonomy/ssai-2020/contest-server.git

2. Install [docker](https://www.docker.com/products/docker-desktop) if you don't already have it

3. `cd` into the project directory and run 

        MIGRATE=true docker-compose up --build
    
    The shell variable `MIGRATE` should be set to true when you set up the server for the first time, as it runs the database migrations. For all future starts, you can run
    
        docker-compose up
    
    Now, you should see in the console a message telling you an admin user has been created, along with a randomly-generated password. Please write down this password, and you'll probably want to change it. If you need to reset the database for some reason, you'll have to install yarn, then `cd backend` and run `yarn install`, and then `npx knex:migrate rollback --all`. After that, to migrate the database again, do `MIGRATE=true docker-compose up`.
    
    Additionally, there are three env files you will need to create: one in the root directory, one in frontend, and the other in backend. In the root directory (`contest-server/.env`), the keys you need to set are:
    
        POSTGRES_USER=
        POSTGRES_PASSWORD=
        NODE_ENV=
        DB_HOST=
        DB_USER=
        DB_PASSWORD=
        DB_DATABASE=
        SESSION_SECRET=
        MIGRATE=${MIGRATE:-false}
        
    In `contest-server/frontend/.env`, you need to set the key `VUE_APP_RECAPTCHA_SITE_KEY` with your public recaptcha key. In `contest-server/backend/.env`, you need to set the key `RECAPTCHA_SECRET` with your private recaptcha key. (You can obtain these keys from Google's recaptcha site.)

        
4. Go to localhost, and you should see the website!
